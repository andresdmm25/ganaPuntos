$(document).ready(function() {


    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 2000, //autoplayHoverPause:true,autoWidth:true, 
        dots: false,
        navText: [
        ],
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                nav: true
            },
            480: {
                items: 1,
                nav: true
            },
            500: {
                items: 1,
                nav: true
            },
            600: {
                items: 4,
                nav: true
            },
            700: {
                items: 4,
                nav: true
            },
            1280: {
                items: 4,
                nav: true
            }
        }
    });

     

     $('.owl-carousel2').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 2000, //autoplayHoverPause:true,autoWidth:true, 
        dots: false,
        navText: [


        ],
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                nav: true
            },
            480: {
                items: 1,
                nav: true
            },
            500: {
                items: 1,
                nav: true
            },
            600: {
                items: 4,
                nav: true
            },
            700: {
                items: 3,
                nav: true
            },
            1280: {
                items:3,
                nav: true
            }
        }
    });



});
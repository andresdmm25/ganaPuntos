<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','viewController@home');

Route::get('/conoceExtencion','viewController@conoceExtencion');

Route::get('/TiendasAsociadas','viewController@TiendasAsociadas');


/*Route::post('/enviarEmail','serviciosEmpresaController@enviarEmail');*/






@extends('MasterTemplate')
@push('titulo')
<title>Conoce la Extencion</title>
@endpush
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/Extencion-responsive.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/Extencion.css') }}">
@endpush

@section('content')
<div class="contenedorExtencion">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-xs-12 col-lg-12">
                <div class="bannerExtencion"></div>
            </div>
        </div>
        <div class="contenedorInterno">
            <div class="row">
                <div class="col-lg-12">
                    <div class="contenedorInformacion">
                        <div class="bannerInformacionLeft">
                        </div>
                        <div class="informacionRight">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="contenedorInformacion">
                    	 <div class="bannerInformacionRigth">
                        </div>
                        <div class="informacionLeft">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                       
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="contenedorInformacion">
                        <div class="bannerInformacionLeft">
                        </div>
                        <div class="informacionRight">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-lg-12">
                    <div class="contenedorInformacion">
                         <div class="bannerInformacionRigth">
                        </div>
                        <div class="informacionLeft">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                       
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
@endsection
@extends('MasterTemplate')

@push('angular')
<script type="text/javascript" src="{{ asset('angularController/tiendas-asociadasController.js') }}"></script>
@endpush 


<link rel="stylesheet" type="text/css" href="{{ asset('css/TiendasAsociadas.css') }}"> 
<link rel="stylesheet" type="text/css" href="{{ asset('css/TiendasAsociadasResponsive.css') }}">


@section('content')

<div class="contenedorTiendas">
    <div class="bannerTiendas"></div>
    <div class="cantidadTiendas">
        <div class="container-fluid">
        	<div class="seccionTiendas">
            	<div class="row">
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/chilemat.jpg') }}"></div></div>
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/kelme.jpg') }}"></div></div>
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/manga-corta.jpg') }}"></div></div>
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/vandine.jpg') }}"></div></div>
            	</div>
        	</div>
          <div class="seccionTiendas">
            	<div class="row ">
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/chilemat.jpg') }}"></div></div>
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/kelme.jpg') }}"></div></div>
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/manga-corta.jpg') }}"></div></div>
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/vandine.jpg') }}"></div></div>
            	</div>
        	</div>
       <div class="seccionTiendas">
            	<div class="row ">
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/chilemat.jpg') }}"></div></div>
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/kelme.jpg') }}"></div></div>
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/manga-corta.jpg') }}"></div></div>
                	<div class="col-xs-12  col-sm-6  col-lg-3"><div class="imgTienda"><img src="{{ asset('img/home/vandine.jpg') }}"></div></div>
            	</div>
        	</div>
        </div>
    </div>
</div>
@endsection
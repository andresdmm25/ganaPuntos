@extends('MasterTemplate')

@push('titulo')
   <title>Home</title>
@endpush

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/home-responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/home.css') }}">
@endpush

@section('content')

<div class="contenidoPag">
        <div class="bannerPuntos">
            <div class="imgBanner"></div>          
        </div>
        <div class="container-fluid">
            <div class="multi">
                <div class="row-fluid">
                    <div class="owl-carousel">
                        <div class="move"><img src="{{ asset('img/home/chilemat.jpg')}}"></div>
                        <div class="move"><img src="{{ asset('img/home/kelme.jpg')}}"></div>
                        <div class="move"><img src="{{ asset('img/home/manga-corta.jpg')}}"></div>
                        <div class="move"><img src="{{ asset('img/home/vandine.jpg')}}"></div>
                        <div class="move"><img src="{{ asset('img/home/chilemat.jpg')}}"></div>
                        <div class="move"><img src="{{ asset('img/home/kelme.jpg')}}"></div>
                        <div class="move"><img src="{{ asset('img/home/manga-corta.jpg')}}"></div>
                        <div class="move"><img src="{{ asset('img/home/vandine.jpg')}}"></div>
                     
                    </div>
                </div>
            </div>
            <div class="btnCanjear">
                <a href="/TiendasAsociadas" style="text-decoration: none;color:#fff;"><button>VER MAS TIENDAS</button></a>
            </div>
            
            <div class="row"  id="promo">  
                <div class="col-lg-12" id="intPromo">
                       <div class="multi2">
                          <div class="row-fluid">
                                <div class="enunciadoPromociones">
                                    <h1>PROMOCIONES</h1>
                                    <div class="lineaPromo"></div>
                                </div>
                                <br>    
                          <!--<div class="owl-carousel2">
                            <div class="move"><img src="{{ asset('img/1.jpg')}}"></div>
                            <div class="move"><img src="{{ asset('img/2.jpg')}}"></div>
                            <div class="move"><img src="{{ asset('img/3.jpg')}}"></div>
                            <div class="move"><img src="{{ asset('img/4.jpg')}}"></div>
                            <div class="move"><img src="{{ asset('img/1.jpg')}}"></div>
                            <div class="move"><img src="{{ asset('img/2.jpg')}}"></div>
                            <div class="move"><img src="{{ asset('img/3.jpg')}}"></div>
                            <div class="move"><img src="{{ asset('img/4.jpg')}}"></div>
                            </div> -->

                        <div class="promos">
                            <div class="imgPromo" id="imgPromo1"></div>
                            <div class="tituloPromo">                               
                                <div class="nombrePromo" id="nombrePromo1"></div>
                                <div class="nombrePromo2">                                   
                                    <h1>GANA PUNTOS DCANJE AQUÍ</h1>
                                </div>
                            </div>
                        </div>

                         <div class="promos">
                            <div class="imgPromo" id="imgPromo2"></div>
                            <div class="tituloPromo">                               
                                <div class="nombrePromo" id="nombrePromo2"></div>
                                <div class="nombrePromo2">                                   
                                    <h1>GANA PUNTOS DCANJE AQUÍ</h1>
                                </div>
                            </div>
                        </div>

                         <div class="promos">
                            <div class="imgPromo" id="imgPromo3"></div>
                            <div class="tituloPromo">                               
                                <div class="nombrePromo" id="nombrePromo3"></div>
                                <div class="nombrePromo2">
                                    <h1>GANA PUNTOS DCANJE AQUÍ</h1>
                                </div>
                            </div>
                        </div>
                 
                   </div>
               </div>             
            </div>
        </div>            
           <div class="row">
                <div class="contenedorPasos">
                    <div class="enunciadoPasos">
                        <h1>CÓMO FUNCIONA</h1>
                        <div class="lineaPromo"></div>
                    </div>
                    <div class="contPasos">
                        <div class="paso">
                            <div class="contImg">
                                <div class="imgPaso" id="imgPaso1">
                                </div>
                            </div>
                            
                            <div class="textoPaso">
                                <p>Compra online en las tiendas asociadas a Dcanje.com. Previa-mente debes estar registrado en nuestro sitio web.</p>
                            </div>
                        </div>
                        <div class="paso">
                            <div class="contImg">
                                <div class="imgPaso" id="imgPaso2">
                                </div>
                            </div>
                            
                            <div class="textoPaso">
                                <p>Recibe un correo electrónico de aviso con la confirmación de tu carga de puntos en tu cuenta Dcanje.com en un plazo máximo de 35 días una vez efectuada la compra</p>
                            </div>
                        </div>
                        <div class="paso">
                            <div class="contImg">
                                <div class="imgPaso" id="imgPaso3">
                                </div>
                            </div>
                            
                            <div class="textoPaso">
                                <p>Cada nueva compra en nuestros comercios asociados te entregará nuevos Puntos Dcanje.</p>
                            </div> 
                        </div>
                        <div class="paso">
                            <div class="contImg">
                                <div class="imgPaso" id="imgPaso4">
                                </div>
                            </div>
                            
                            <div class="textoPaso">
                                <p>Canjea y disfruta tus puntos canjeando en Dcanje.com por cientos de Gift Cards, Viajes y Productos en línea. También acumulas puntos en tiendas  que entregan puntos a través de nuestra extensión Google Chrome.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="btnCanjear">
                <a href="/conoceExtencion"><button>CONOCE Y DESCARGA LA EXTENSIÓN GOOGLE CHROME</button></a>
            </div>
        </div>
       </div>
    </div>

    @endsection 

    @push('js')
        <script src="{{ asset('js/owl.carousel.min.js ') }}"></script>
        <script src="{{ asset('js/MiniS2.js ') }}"></script>
    @endpush
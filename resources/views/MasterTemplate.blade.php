<!DOCTYPE html>
<html lang="en" ng-app="app">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    @stack('titulo')
    
    <link rel="stylesheet" href="{{ asset('css/MasterEstilo.css') }}">

    @stack('css')

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/angular-ui-carousel/dist/ui-carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/bower_components/ng-tags-input/ng-tags-input.min.css') }}"> 
    <!-- estilos carousel -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css ') }}">
    <!-- estilos carousel -->
    <!-- script de angular js -->
    <script type="text/javascript" src="{{ asset('bower_components/angular/angular.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/bower_components/ng-tags-input/ng-tags-input.min.js') }}"></script>


    <script type="text/javascript" src="{{ asset('angularController/MainController.js') }}"></script>
    @stack('angular')
    <script type="text/javascript" src="{{ asset('bower_components/angular-bootstrap/ui-bootstrap.min.js') }} "></script>
    <!-- script de angular js -->
</head>

<body>

<!-- <div class="popupEcommerce" ng-controller="popController">

    <div class="internoE">

        <div class="cerrarPopup">
            
            <p>x</p>

        </div>


        <div class="headerE">

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

        </div>
        
        <div class="contenidoPopup">
            <div class="bannerPopup">
            </div>
            <div class="formularioPop">
                <form class="form-vertical"  method="post">
                    <div class="form-group">
                        <label for="">From</label>
                        <input type="text" name="" ng-model="From">
                    </div>
                    <div class="form-group">
                        <label for="">Subject</label>
                        <input type="text" name="" ng-model="Subject">
                    </div>
                    <div class="form-group">
                        <label for="">To</label>
                        <input type="text" name="" ng-model="To">
                    </div>
                    <div class="form-group">
                        <label for="">CC</label>
                        <input type="text" name="" ng-model="CC">
                    </div>
                    <div class="form-group">
                        <label for="">BCC</label>
                        <input type="text" name="" ng-model="BCC" >
                    </div>
                    <div class="form-group">                    
                        <textarea class="form-control" ng-model="escritura"></textarea>
                        <input type="hidden" name="_token" ng-model="token" value="{{ csrf_token() }}">
                    </div>
                                                                       
                </form>

                <button class="btn btn-success" ng-click="enviarEmail()" >Enviar</button>

            </div>
        </div>
    </div>
</div>
 -->

    <!--  header -->
   
<!--   <div class="container-fluid">
    <div class="header">
        <div class="logoHeader">
        </div>
        <div class="menuHeader">
            <ul>
                <li> <a href="/TiendasAsociadas">TIENDAS ASOCIADAS</a></li>
                <li>PROMOCIONES</li>
                <li>COMO FUNCIONA</li>
                <li> <a href="/conoceExtencion">CONOCE LA EXTENSIÓN</a></li>
                <li  ng-click="inscribirEcommerce()">INSCRIBIR E-COMMERCE</li>
            </ul>
        </div>
    </div>
</div>
 -->

    <!-- header -->

    <!-- body -->


    @yield('content')


    <!-- body -->


    <!-- footer -->
    <div class="footer"></div>
    <!-- footer -->
    <script src="{{ asset('/js/jquery-3.3.1.min.js ') }}"></script>
    @stack('js')
</body>

</html>
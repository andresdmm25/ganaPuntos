<?php
namespace App\Classes;

class emailer 
{
    private $plantilla,$datos,$email,$subject,$emailSender,$nameSender,$attachFile = [];
    function __construct($plantilla = null, $datos = null)
    {
        if(strlen($plantilla)>0 && strlen($datos)>0)
        {
            $this->setBaseEmail($plantilla,$datos);     
        } 
        $this->emailSender=getenv('MAIL_SENDER');
        $this->nameSender=getenv('MAIL_DOMAIN'); 
    }
    public function setBaseEmail($plantilla,$datos)
    {
        $this->plantilla = $plantilla;
        $this->datos = $datos;
    }
    public function setPlantillaEmail($plantilla)
    {
        $this->plantilla = $plantilla;
    }
    public function setDatosEmail($datos)
    {
        $this->datos = $datos;
    }
    public function setEmailAndSend($email,$subject)
    {
        $this->email = $email;
        $this->subject = $subject;
        $this->enviarMail();
    }
    public function setSender($emailSender,$nameSender){
        $this->emailSender=$emailSender;
        $this->nameSender=$nameSender;
    }
    private function enviarMail()
    {
        \Mail::send($this->plantilla,$this->datos, function ($message) {
            $message->from($this->emailSender, $this->nameSender)->to($this->email)->subject($this->subject);
        }); 
    }
    /* $attachFile -> array, debe tener información : filename, mime,path */
    public function setAttachFileEmailAndSend($email,$subject,$attachFile)
    { 
        $this->attachFile = $attachFile;
        $this->email = $email;
        $this->subject = $subject;
        $this->enviarMailAttachFile();
    }
    private function enviarMailAttachFile()
    {
        \Mail::send($this->plantilla,$this->datos, function ($message) {
            foreach ($this->attachFile as $key => $file) {
                $message->attach($file['path'], ['as' => $file['filename'], 'mime' => $file['mime']]);
            } 
            $message->from($this->emailSender, $this->nameSender)->to($this->email)->subject($this->subject);
        }); 
    }
}

?>
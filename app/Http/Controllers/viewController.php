<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class viewController extends Controller
{
   
	public function home(){

      return view('HomeTemplate');
      
    }

    public function conoceExtencion(){

      return view('ConoceLaExtencion');
      
    }

    public function TiendasAsociadas(){

      return view('TiendasAsociadas');
      
    }



}
